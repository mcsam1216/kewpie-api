FROM node:latest

COPY src /nodejs/src
COPY package.json /nodejs/
COPY .babelrc /nodejs/

WORKDIR /nodejs

RUN npm install --only=prod
RUN npm run build

ENV NODE_ENV=prod
ENV PORT=3000
ENV MONGODB_URI=mongodb://database_mongo:27017/kewpie

ENTRYPOINT [ "npm", "start" ]

