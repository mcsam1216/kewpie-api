module.exports = {
  transform: {
    '^.+\\.[t|j]sx?$': 'babel-jest',
  },
  testPathIgnorePatterns: [
    '/node_modules/',
  ],
  testEnvironment : 'node',
  setupFiles      : [
    'dotenv/config',
  ],
  setupFilesAfterEnv: [
    '<rootDir>/src/config/jest-setup.js',
  ],
};