import { Schema, model } from 'mongoose';

const productSchema = new Schema({
  productName: {
    type     : String,
    required : true,
  },
  productCategory: {
    type     : Schema.Types.ObjectId,
    ref      : 'ProductCategory',
    required : true,
  },
  productTitle: {
    type     : String,
    required : true,
  },
  productInfo: {
    type     : String,
    required : true,
  },
  availableIn: {
    type     : String,
    required : true,
  },
  mainIngredient: {
    type     : String,
    required : true,
  },
  description: {
    type     : String,
    required : true,
  },
});

productSchema.pre('remove', function (next) {
  this.model('Media').deleteMany({ referenceID: this._id }, next);
});

export default model('Product', productSchema);
