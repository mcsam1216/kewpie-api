import mongoose from 'mongoose';
import Product from './product';
import ProductCategory from './product-category';
import FoodService from './food-service';
import FoodServiceCategory from './food-service-category';

mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser    : true,
  useUnifiedTopology : true,
});
const dbConnection = mongoose.connection;
dbConnection.on('error', (error) => console.error(error)); // eslint-disable-line no-console
dbConnection.once('open', () => console.log(`Connected to database : ${process.env.MONGODB_URI}`)); // eslint-disable-line no-console

const models = {
  Product,
  ProductCategory,
  FoodService,
  FoodServiceCategory,
};

export { dbConnection };
export default models;
