import mongoose from 'mongoose';

export const referenceTypes = Object.freeze({
  PRODUCT               : 'product',
  PRODUCT_CATEGORY      : 'productCategory',
  FOOD_SERVICE          : 'foodService',
  FOOD_SERVICE_CATEGORY : 'foodServiceCategory',
});

export const mediaTypes = Object.freeze({
  IMAGE : 'image',
  VIDEO : 'video',
});

const mediaSchema = new mongoose.Schema({
  referenceID: {
    type     : String,
    required : true,
  },
  referenceType: {
    type     : String,
    enum     : Object.keys(referenceTypes).map((key) => referenceTypes[key]),
    required : true,
  },
  mediaType: {
    type : String,
    enum : Object.keys(mediaTypes).map((key) => mediaTypes[key]),
  },
  mediaPath: {
    type: String,
  },
});

export default mongoose.model('Media', mediaSchema);
