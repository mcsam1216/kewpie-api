import mongoose from 'mongoose';

const productCategorySchema = new mongoose.Schema({
  categoryTitle: {
    type     : String,
    required : true,
  },
  categoryDescription: {
    type: String,
  },
  categoryImage: {
    type: String,
  },
});

productCategorySchema.pre('remove', function (next) {
  this.model('Product').deleteMany({ productCategory: this._id }, next);
});

export default mongoose.model('ProductCategory', productCategorySchema);
