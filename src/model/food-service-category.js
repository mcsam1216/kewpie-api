import mongoose from 'mongoose';

const foodServiceCategorySchema = new mongoose.Schema({
  categoryTitle: {
    type     : String,
    required : true,
  },
  categoryDescription: {
    type: String,
  },
  categoryImage: {
    type: String,
  },
});

export default mongoose.model('FoodServiceCategory', foodServiceCategorySchema);
