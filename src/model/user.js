import mongoose from 'mongoose';
import bcrypt from 'bcrypt';

const userSchema = new mongoose.Schema({
  username: {
    type     : String,
    required : true,
  },
  password: {
    type     : String,
    required : true,
  },
  lastLogin: {
    type     : Date,
    required : true,
  },
  lastIP: {
    type     : String,
    required : true,
  },
});

userSchema.set('toJSON', {
  transform(doc, ret) {
    delete ret.password;
    return ret;
  },
});

userSchema.pre('save', function (next) {
  const self = this;
  if (!this.isModified('password')) return next();

  bcrypt.hash(this.password, 10, (err, hash) => {
    if (err) return next(err);

    self.password = hash;
    next();
  });
});

userSchema.methods.getTokenData = function () {
  return {
    id       : this._id,
    username : this.username,
  };
};

userSchema.methods.verifyPassword = function (candidatePassword, callback) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) return callback(err);
    callback(null, isMatch);
  });
};

module.exports = mongoose.model('User', userSchema);
