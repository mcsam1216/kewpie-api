import mongoose from 'mongoose';

const foodServiceSchema = new mongoose.Schema({
  foodServiceName: {
    type     : String,
    required : true,
  },
  foodServiceFullName: {
    type     : String,
    required : true,
  },
  foodServiceCategory: {
    type     : String,
    required : true,
  },
  foodServiceInfo: {
    type     : String,
    required : true,
  },
  availableIn: {
    type     : String,
    required : true,
  },
  ambient: {
    type     : String,
    required : true,
  },
  foodServiceDescription: {
    type     : String,
    required : true,
  },
  foodServiceImageThumb: {
    type     : String,
    required : true,
  },
  foodServiceImageBig: {
    type     : String,
    required : true,
  },
});

export default mongoose.model('FoodService', foodServiceSchema);
