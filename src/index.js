import express from 'express';
import cors from 'cors';
import morgan from 'morgan';
import compression from 'compression';
import helmet from 'helmet';
import path from 'path';
import routes from './routes';
import { fileUploadConfig } from './config/file-upload';
import config from './config';
import { dbConnection } from './model';

const app = express();
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use(fileUploadConfig);
app.use(cors());
app.use(morgan('dev'));

if (process.env.NODE_ENV === 'prod') {
  app.use(compression());
}
app.use(helmet());
app.use(config.file.download, express.static(path.join(__dirname, '..', '..', 'uploads')));
app.use('/', routes);

const server = app.listen(process.env.PORT, () => {
  console.log(`Services started : listening on port ${process.env.PORT}`); // eslint-disable-line no-console
});

export { server, dbConnection };
