export const sendCreated = (res, data) => res.status(201).send(data);

export const sendToken = (res, token) => res.status(200).json({
  success : true,
  message : 'Token created',
  token,
});

export const sendServerError = (res, message) => res.status(500).send({
  success: false,
  message,
});

export const sendBadRequest = (res, message) => res.status(400).send({
  success: false,
  message,
});

export const sendUnauthorized = (res, message) => res.status(401).send({
  success: false,
  message,
});

export const sendForbidden = (res) => res.status(403).send({
  success : false,
  message : 'You do not have rights to access this resource.',
});

export const sendNotFound = (res) => res.status(404).send({
  success : false,
  message : 'Resource not found.',
});

export const setHeadersForCORS = (req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, X-Access-Token, Content-Type, Accept',
  );
  next();
};
