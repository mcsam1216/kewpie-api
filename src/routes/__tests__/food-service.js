import { request, token } from '../../config/jest-setup';
import config from '../../config';

let foodServiceID;

const foodServiceSample = {
  foodServiceName        : expect.any(String),
  foodServiceFullName    : expect.any(String),
  foodServiceCategory    : expect.any(String),
  foodServiceInfo        : expect.any(String),
  availableIn            : expect.any(String),
  ambient                : expect.any(String),
  foodServiceDescription : expect.any(String),
  foodServiceImageThumb  : expect.any(String),
  foodServiceImageBig    : expect.any(String),
};

describe('Food Sevice\' Routes', () => {
  test('Get Food Service', async (done) => {
    const { status, body } = await request
      .get(`/v${config.version}/foodService`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    body.forEach((foodService) => {
      expect(foodService).toMatchObject(foodServiceSample);
    });
    done();
  });

  test('Post Food Service', async (done) => {
    const { status, body } = await request
      .post(`/v${config.version}/foodService`)
      .send({
        foodServiceName        : 'Dummy Name',
        foodServiceFullName    : 'Dummy Full Name',
        foodServiceCategory    : '5e3c1ad700a273397e5964ac',
        foodServiceInfo        : 'Dummy Info',
        availableIn            : 'Dummy Available',
        ambient                : 'Dummy Ambient',
        foodServiceDescription : 'Dummy Description',
        foodServiceImageThumb  : '/path/to/image.png',
        foodServiceImageBig    : '/path/to/image.png',
      })
      .set('x-access-token', token);

    foodServiceID = body._id;
    expect(status).toBe(201);
    expect(body).toMatchObject(foodServiceSample);
    done();
  });

  test('Get Food Service by CategoryID', async (done) => {
    const { status, body } = await request
      .get(`/v${config.version}/foodService/cat/5e3c1ad700a273397e5964ac`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    body.forEach((foodService) => {
      expect(foodService).toMatchObject(foodServiceSample);
    });
    done();
  });

  test('Update Food Service', async (done) => {
    const { status, body } = await request
      .patch(`/v${config.version}/foodService/${foodServiceID}`)
      .send({
        foodServiceName: 'Update Dummy Name',
      })
      .set('x-access-token', token);

    expect(status).toBe(200);
    expect(body).toMatchObject(foodServiceSample);
    done();
  });

  test('Get Food Service by ID', async (done) => {
    const { status, body } = await request
      .get(`/v${config.version}/foodService/${foodServiceID}`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    expect(body).toMatchObject(foodServiceSample);
    done();
  });

  test('Delete Food Service by ID', async (done) => {
    const { status, body } = await request
      .delete(`/v${config.version}/foodService/${foodServiceID}`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    expect(body.message).toBe('Food service deleted');
    done();
  });
});
