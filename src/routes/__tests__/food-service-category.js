import { request, token } from '../../config/jest-setup';
import config from '../../config';

let foodServiceCategoryID;

const foodServiceCategorySample = {
  categoryTitle       : expect.any(String),
  categoryDescription : expect.any(String),
  categoryImage       : expect.any(String),
};

describe('Product Category\' Routes', () => {
  it('Get Food Service\'s Categories', async done => {
    const { status, body } = await request
      .get(`/v${config.version}/foodServiceCategory`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    body.forEach((foodServiceCategory) => {
      expect(foodServiceCategory).toMatchObject(foodServiceCategorySample);
    });
    done();
  });

  it('Post Food Service\'s Category', async done => {
    const { status, body } = await request
      .post(`/v${config.version}/foodServiceCategory`)
      .send({
        categoryTitle       : 'Dummy Title',
        categoryDescription : '5e3c1ad700a273397e5964ac',
        categoryImage       : '/path/to/image.png',
      })
      .set('x-access-token', token);

    foodServiceCategoryID = body._id;
    expect(status).toBe(201);
    expect(body).toMatchObject(foodServiceCategorySample);
    done();
  });

  it('Update Food Service\'s Categpry', async done => {
    const { status, body } = await request
      .patch(`/v${config.version}/foodServiceCategory/${foodServiceCategoryID}`)
      .send({
        categoryTitle: 'Update Dummy Title',
      })
      .set('x-access-token', token);

    expect(status).toBe(200);
    expect(body).toMatchObject(foodServiceCategorySample);
    done();
  });

  it('Get Food Service\'s Categpry by ID', async done => {
    const { status, body } = await request
      .get(`/v${config.version}/foodServiceCategory/${foodServiceCategoryID}`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    expect(body).toMatchObject(foodServiceCategorySample);
    done();
  });

  it('Delete Food Service\'s Category by ID', async done => {
    const { status, body } = await request
      .delete(`/v${config.version}/foodServiceCategory/${foodServiceCategoryID}`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    expect(body.message).toBe('Food service\'s category deleted');
    done();
  });
});
