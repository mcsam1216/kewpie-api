import { request } from '../../config/jest-setup';

describe('Authentication Routes', () => {
    it('Login', async done => {
        const { status, body } = await request
        .post(`/auth/token`)
        .send({
            username: 'admin',
            password: '123456'
        });

        expect(status).toBe(200);
        expect(typeof body.token).toBe('string');
        done();
    })
});