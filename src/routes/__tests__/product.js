import { request, token } from '../../config/jest-setup';
import config from '../../config';
import path from 'path';

let productID;

const productSample = {
  productName       : expect.any(String),
  productCategory   : expect.any(Object),
  productTitle      : expect.any(String),
  productInfo       : expect.any(String),
  availableIn       : expect.any(String),
  mainIngredient    : expect.any(String),
  description       : expect.any(String),
  productImages     : expect.any(Array),
};

describe('Product Routes POST', () => {
  it('Post Product', async (done) => {
    const { status, body } = await request
      .post(`/v${config.version}/product`)
      .field('productName', 'Dummy Name')
      .field('productCategory', '5e4a97bd6d5f0d0fc3b9e9de')
      .field('productTitle', 'Dummy Title')
      .field('productInfo', 'Dummy Info')
      .field('availableIn', 'Dummy ml')
      .field('mainIngredient', 'Dummy Ingredient')
      .field('description', 'Dummy Description')
      .attach('productImages', path.resolve(__dirname, 'files', 'productThumb.png'))
    .set('x-access-token', token);

    productID = body._id;
    expect(status).toBe(201);
    expect(body).toMatchObject(productSample);
    done();
  });
});

describe('Product Routes PATCH', () => {
  it('Update Product', async (done) => {
    const { status, body } = await request
      .patch(`/v${config.version}/product/${productID}`)
      .field("productName", 'Update Dummy Name')
      .attach('productImages', path.resolve(__dirname, 'files', 'productThumb.png'))
      .attach('productImages', path.resolve(__dirname, 'files', 'productBig.png'))
      .set('x-access-token', token);

    expect(status).toBe(200);
    expect(body).toMatchObject(productSample);
    done();
  });
});

describe('Product Routes GET', () => {
  it('Get Products', async (done) => {
    const { status, body } = await request
      .get(`/v${config.version}/product`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    body.forEach((product) => {
      expect(product).toMatchObject(productSample);
    });
    done();
  });
  it('Get Product by ID', async (done) => {
    const { status, body } = await request
      .get(`/v${config.version}/product/${productID}`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    expect(body).toMatchObject(productSample);
    done();
  });
  it('Get Product by CategoryID', async (done) => {
    const { status, body } = await request
      .get(`/v${config.version}/product/cat/5e3c1ad700a273397e5964ac`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    body.forEach((product) => {
      expect(product).toMatchObject(productSample);
    });
    done();
  });
});

describe('Product Routes DELETE', () => {
  it('Delete Product by ID', async (done) => {
    const { status, body } = await request
      .delete(`/v${config.version}/product/${productID}`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    expect(body.message).toBe('Product deleted');
    done();
  });
});
