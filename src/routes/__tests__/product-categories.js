import { request, token } from '../../config/jest-setup';
import config from '../../config';

let productcategoryID;

const productCateogrySample = {
  categoryTitle       : expect.any(String),
  categoryDescription : expect.any(String),
  categoryImage       : expect.any(String),
};

describe('Product Category\' Routes', () => {
  it('Get Product\'s Categories', async (done) => {
    const { status, body } = await request
      .get(`/v${config.version}/productCategory`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    body.forEach((productCategory) => {
      expect(productCategory).toMatchObject(productCateogrySample);
    });
    done();
  });

  it('Post Product\'s Category', async (done) => {
    const { status, body } = await request
      .post(`/v${config.version}/productCategory`)
      .send({
        categoryTitle       : 'Dummy Title',
        categoryDescription : '5e3c1ad700a273397e5964ac',
        categoryImage       : '/path/to/image.png',
      })
      .set('x-access-token', token);

    productcategoryID = body._id;
    expect(status).toBe(201);
    expect(body).toMatchObject(productCateogrySample);
    done();
  });

  it('Update Product\'s Categpry', async (done) => {
    const { status, body } = await request
      .patch(`/v${config.version}/productCategory/${productcategoryID}`)
      .send({
        categoryTitle: 'Update Dummy Title',
      })
      .set('x-access-token', token);

    expect(status).toBe(200);
    expect(body).toMatchObject(productCateogrySample);
    done();
  });

  it('Get Product\'s Categpry by ID', async (done) => {
    const { status, body } = await request
      .get(`/v${config.version}/productCategory/${productcategoryID}`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    expect(body).toMatchObject(productCateogrySample);
    done();
  });

  it('Delete Product\'s Category by ID', async (done) => {
    const { status, body } = await request
      .delete(`/v${config.version}/productCategory/${productcategoryID}`)
      .set('x-access-token', token);

    expect(status).toBe(200);
    expect(body.message).toBe('Product\'s category deleted');
    done();
  });
});
