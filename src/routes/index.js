import express from 'express';
import { setHeadersForCORS, sendNotFound } from '../helpers/response';

import auth from './auth';
import protectedRoute from './protected';
import user from './user';

import { verifyToken } from '../controllers';
import config from '../config';

const routes = express.Router();

routes.use(setHeadersForCORS);

routes.use('/auth', auth);
routes.use('/user', user);
routes.use(`/v${config.version}`, verifyToken, protectedRoute);

routes.get('/', (req, res) => res.status(200).json({ success: true, message: 'OK' }));

routes.use('*', (req, res) => sendNotFound(res));

export default routes;
