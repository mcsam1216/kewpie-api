import express from 'express';
import { postUser } from '../controllers';

const router = express.Router();

router.route('/').post(postUser);

export default router;
