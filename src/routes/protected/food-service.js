import express from 'express';
import {
  deleteFoodService,
  getFoodService,
  getFoodServiceByCategory,
  getFoodServices,
  postFoodService,
  updateFoodService,
} from '../../controllers';

const router = express.Router();

router
  .route('/')
  .get(getFoodServices)
  .post(postFoodService);

router.get('/cat/:category_id', getFoodServiceByCategory);

router
  .route('/:id')
  .all(getFoodService)
  .get((req, res) => {
    res.json(res.foodService);
  })
  .patch(updateFoodService)
  .delete(deleteFoodService);

export default router;
