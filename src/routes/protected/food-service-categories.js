import express from 'express';
import {
  deleteFoodServiceCategory,
  getFoodServiceCategories,
  getFoodServiceCategory,
  postFoodServiceCategory,
  updateFoodServiceCategory,
} from '../../controllers';

const router = express.Router();

router
  .route('/')
  .get(getFoodServiceCategories)
  .post(postFoodServiceCategory);

router
  .route('/:id')
  .all(getFoodServiceCategory)
  .get((req, res) => {
    res.json(res.foodServiceCategory);
  })
  .patch(updateFoodServiceCategory)
  .delete(deleteFoodServiceCategory);

export default router;
