import express from 'express';
// import userRouter from './user';
import productRouter from './products';
import productCategoryRouter from './product-categories';
import foodServiceRouter from './food-service';
import foodServiceCategoryRouter from './food-service-categories';

const routes = express.Router();

routes.use('/product', productRouter);
routes.use('/productCategory', productCategoryRouter);
routes.use('/foodService', foodServiceRouter);
routes.use('/foodServiceCategory', foodServiceCategoryRouter);

export default routes;
