import express from 'express';
import {
  checkProductImages,
  deleteProduct,
  getProduct,
  getProductByID,
  getProductByCategory,
  getProducts,
  postProduct,
  removeProductMedia,
  updateProduct,
} from '../../controllers';

const router = express.Router();

router
  .route('/')
  .get(getProducts)
  .post(checkProductImages, postProduct);

router
  .route('/:id')
  .all(getProduct)
  .get(getProductByID)
  .patch(checkProductImages, removeProductMedia, updateProduct)
  .delete(deleteProduct);

router.get('/cat/:category_id', getProductByCategory);

export default router;
