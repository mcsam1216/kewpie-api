import express from 'express';
import {
  deleteProductCategory,
  getProductCategories,
  getProductCategory,
  postProductCategory,
  updateProductCategory,
} from '../../controllers';

const router = express.Router();
// Get all categories
router
  .route('/')
  .get(getProductCategories)
  .post(postProductCategory);

// Get one productCategory
router
  .route('/:id')
  .all(getProductCategory)
  .get((req, res) => {
    res.json(res.productCategory);
  })
  .patch(updateProductCategory)
  .delete(deleteProductCategory);

export default router;
