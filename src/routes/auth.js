import express from 'express';
import { login } from '../controllers';

const routes = express.Router();

routes.post('/token', login);

export default routes;
