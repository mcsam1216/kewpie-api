import path from 'path';

export default {
  version : 1,
  jwt     : {
    secretKey: '37LvDSm4XvjYOh9Y',
  },
  testUser: {
    username : 'admin',
    password : '123456',
  },
  file: {
    upload   : path.resolve(__dirname, '..', 'uploads'),
    download : 'static',
  },
};
