import fileUpload from 'express-fileupload';
import { sendBadRequest } from '../helpers/response';

let fileUploadConifg = {
  limits             : { fileSize: 50 * 1024 * 1024 },
  createParentPath   : true,
  debug              : process.env.NODE_ENV !== 'prod',
  uriDecodeFileNames : true,
  safeFileNames      : /\s/g,
  limitHandler       : (req, res) => sendBadRequest(res, 'File\'s size exceed limit'),
};

if (process.env.NODE_ENV === 'test') {
  fileUploadConifg = {
    useTempFiles : true,
    tempFileDir  : '/tmp/',
    ...fileUploadConifg,
  };
}

export const fileUploadConfig = fileUpload(fileUploadConifg);

export const saveFile = async (file, filePath) => {
  file.mv(filePath, (err) => {
    if (err) {
      throw new Error(err);
    }
  });
};
