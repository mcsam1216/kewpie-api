
import 'regenerator-runtime/runtime';
import supertest from 'supertest';
import { dbConnection, server } from '../index';
import config from '../config';

const request = supertest(server);
const user = config.testUser;
let token;

beforeAll(async () => {
  await request.post('/user').send(user);
  ({
    body: { token },
  } = await request.post('/auth/token').send(user));
});

afterAll(async () => {
  await server.close();
  await dbConnection.close();
});

export { request, token };
