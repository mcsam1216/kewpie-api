import jwt from 'jsonwebtoken';
import User from '../model/user';
import {
  sendToken, sendUnauthorized, sendServerError, sendNotFound,
} from '../helpers/response';
import config from '../config';

const { secretKey } = config.jwt;

export const login = async (req, res) => {
  const user = await User.findOne({ username: req.body.username });

  if (!user) {
    return sendNotFound(res, 'No user Found');
  }

  user.verifyPassword(req.body.password, (error, isMatch) => {
    if (error) {
      return sendServerError(res, error.message);
    }
    
    if (!isMatch) {
      return sendUnauthorized(res, 'Incorrect password');
    }

    const token = jwt.sign(user.getTokenData(), secretKey);

    return sendToken(res, token);
  });
};

export const verifyToken = async (req, res, next) => {
  const token = req.body.token || req.query.token || req.headers['x-access-token'];

  if (req.method === 'GET') {
    return next();
  }

  if (!token) {
    return sendUnauthorized(res, 'No token provided.');
  }

  jwt.verify(token, secretKey, async (err, decoded) => {
    if (err) {
      return sendUnauthorized(res, err.message);
    }

    try {
      const user = await User.findById(decoded.id);
      req.currentUser = user;
      next();
    } catch (error) {
      return sendUnauthorized(res, error.message);
    }
  });
};
