import { login, verifyToken } from './auth';

import { postUser } from './user';

import {
  deleteProduct,
  getProduct,
  getProductByID,
  getProductByCategory,
  getProducts,
  postProduct,
  updateProduct,
} from './product';

import {
  deleteProductCategory,
  getProductCategories,
  getProductCategory,
  postProductCategory,
  updateProductCategory,
} from './product-category';

import {
  deleteFoodService,
  getFoodService,
  getFoodServiceByCategory,
  getFoodServices,
  postFoodService,
  updateFoodService,
} from './food-service';

import {
  deleteFoodServiceCategory,
  getFoodServiceCategories,
  getFoodServiceCategory,
  postFoodServiceCategory,
  updateFoodServiceCategory,
} from './food-service-category';

import {
  removeProductMedia,
  checkProductImages,
} from './media';

module.exports = {
  /** Authentication */
  login,
  verifyToken,
  /** User */
  postUser,
  /** Product */
  getProduct,
  getProducts,
  getProductByID,
  getProductByCategory,
  postProduct,
  updateProduct,
  deleteProduct,
  /** Product's Category */
  getProductCategories,
  getProductCategory,
  updateProductCategory,
  deleteProductCategory,
  postProductCategory,
  /** Food Service */
  getFoodServices,
  getFoodService,
  postFoodService,
  getFoodServiceByCategory,
  updateFoodService,
  deleteFoodService,
  /** Food Service's Categpry */
  getFoodServiceCategories,
  getFoodServiceCategory,
  postFoodServiceCategory,
  updateFoodServiceCategory,
  deleteFoodServiceCategory,
  /** Media */
  removeProductMedia,
  checkProductImages,
};
