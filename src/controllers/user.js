import User from '../model/user';
import { sendBadRequest, sendCreated } from '../helpers/response';

// eslint-disable-next-line import/prefer-default-export
export const postUser = async (req, res) => {
  const ip = req.headers['x-forwarded-for'] || req.connection.remoteAddress;
  const {
    body: { username, password },
  } = req;
  const user = new User({
    username,
    password,
    lastLogin : Date.now(),
    lastIP    : ip,
  });

  try {
    const newUser = await user.save();
    sendCreated(res, newUser);
  } catch (err) {
    sendBadRequest(res, err.message);
  }
};
