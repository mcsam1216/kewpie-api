import ProductCategory from '../model/product-category';
import { sendServerError, sendBadRequest } from '../helpers/response';

export const getProductCategories = async (req, res) => {
  try {
    const productCategory = await ProductCategory.find();
    res.json(productCategory);
  } catch (err) {
    sendServerError(res, err.message);
  }
};

export const getProductCategorybyID = async (categoryID) => {
  try {
    const productCategory = await ProductCategory.findById(categoryID);

    return productCategory;
  } catch (err) {
    throw new Error(err.message);
  }
};

export const getProductCategory = async (req, res, next) => {
  let productCategory;
  try {
    productCategory = await getProductCategorybyID(req.params.id);
    if (productCategory === null) {
      return res.status(404).json({ message: 'No product\'s category found.' });
    }
  } catch (err) {
    return sendServerError(res, err.message);
  }

  res.productCategory = productCategory;
  next();
};

export const updateProductCategory = async (req, res) => {
  for (const prop in req.body) {
    res.productCategory[prop] = req.body[prop];
  }

  try {
    const updateProduct = await res.productCategory.save();
    res.json(updateProduct);
  } catch (err) {
    sendBadRequest(res, err.message);
  }
};

export const deleteProductCategory = async (req, res) => {
  try {
    await res.productCategory.remove();
    res.json({ message: 'Product\'s category deleted' });
  } catch (err) {
    sendServerError(res, err.message);
  }
};

export const postProductCategory = async (req, res) => {
  const { body } = req;
  const productCategory = new ProductCategory(body);

  try {
    const newCategoryProduct = await productCategory.save();
    res.status(201).json(newCategoryProduct);
  } catch (err) {
    sendBadRequest(res, err.message);
  }
};
