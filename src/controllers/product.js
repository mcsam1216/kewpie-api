import fs from 'fs';
import Product from '../model/product';
import { createNewProductMedia, getProductImages } from './media';
import { getProductCategorybyID } from './product-category';
import handleError from './error';
import config from '../config';

const productUpload = `${config.file.upload}/products/`;

const injectExtras = async (product) => {
  try {
    product.productImages = await getProductImages(product._id);
    product.productCategory = await getProductCategorybyID(product.productCategory);

    return product;
  } catch (err) {
    throw new Error(err.message);
  }
};

const injectExtrasforArray = async (products) => {
  try {
    for (const product of products) {
      await injectExtras(product);
    }

    return products;
  } catch (err) {
    throw new Error(err.message);
  }
};

export const getProducts = async (req, res) => {
  try {
    const products = await Product.find().lean();
    
    res.json(await injectExtrasforArray(products));
  } catch (err) {
    handleError(res, err);
  }
};

export const getProductByCategory = async (req, res) => {
  try {
    const products = await Product.find({
      productCategory: req.params.category_id,
    }).lean();
    res.json(await injectExtrasforArray(products));
  } catch (err) {
    handleError(res, err);
  }
};

export const getProduct = async (req, res, next) => {
  let product;
  try {
    product = await Product.findById(req.params.id);
    if (!product) {
      return res.status(404).json({ message: 'No product found.' });
    }
  } catch (err) {
    return handleError(res, err);
  }

  res.product = product;
  res.productImages = await getProductImages(product._id);
  next();
};

export const getProductByID = async (req, res) => {
  try {
    res.json(await injectExtras(res.product.toObject()));
  } catch (err) {
    return handleError(res, err);
  }
};

export const postProduct = async (req, res) => {
  try {
    const { body, productImages } = req;
    const product = new Product(body);
    const newProduct = (await product.save()).toObject();
    
    await createNewProductMedia(newProduct._id, productImages);
    
    res.status(201).json(await injectExtras(newProduct));
  } catch (err) {
    handleError(res, err);
  }
};

export const updateProduct = async (req, res) => {
  try {
    const { body, productImages } = req;

    await createNewProductMedia(res.product._id, productImages);

    for (const prop in body) {
      res.product[prop] = body[prop];
    }
    
    const updatedProduct = (await res.product.save()).toObject();
    res.json(await injectExtras(updatedProduct));
  } catch (err) {
    handleError(res, err);
  }
};

export const deleteProduct = async (req, res) => {
  try {
    const { productImages } = res;
    for (const image of productImages) {
      fs.unlinkSync(`${productUpload}${image.mediaPath}`);
    }

    await res.product.remove();
    res.json({ message: 'Product deleted' });
  } catch (err) {
    handleError(res, err);
  }
};