import { sendBadRequest } from '../helpers/response';

export default (res, err) => {
  console.error(err); // eslint-disable-line
  sendBadRequest(res, err.message);
};