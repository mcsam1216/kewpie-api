import FoodService from '../model/food-service';
import FoodServiceCategory from '../model/food-service-category';
import { sendBadRequest, sendServerError } from '../helpers/response';

export const getFoodServices = async (req, res) => {
  try {
    const foodServices = await FoodService.find();
    foodServices.forEach(async (foodService) => {
      const foodServiceCategory = await FoodServiceCategory.findById(
        foodService.foodServiceCategory,
      );
      foodService.categoryName = foodServiceCategory;
    });
    res.json(foodServices);
  } catch (err) {
    sendServerError(res, err.message);
  }
};

export const getFoodService = async (req, res, next) => {
  let foodService;
  try {
    foodService = await FoodService.findById(req.params.id);
    if (foodService === null) {
      return res.status(404).json({ message: 'No food service found.' });
    }
  } catch (err) {
    return sendServerError(res, err.message);
  }

  res.foodService = foodService;
  next();
};

export const getFoodServiceByCategory = async (req, res) => {
  try {
    const foodServices = await FoodService.find({
      foodServiceCategory: req.params.category_id,
    });
    res.json(foodServices);
  } catch (err) {
    sendServerError(res, err.message);
  }
};

export const postFoodService = async (req, res) => {
  const { body } = req;
  const foodService = new FoodService(body);

  try {
    const newFoodService = await foodService.save();
    res.status(201).json(newFoodService);
  } catch (err) {
    sendBadRequest(res, err.message);
  }
};

export const updateFoodService = async (req, res) => {
  for (const prop in req.body) {
    res.foodService[prop] = req.body[prop];
  }

  try {
    const updateFoodSerivce = await res.foodService.save();
    res.json(updateFoodSerivce);
  } catch (err) {
    sendBadRequest(res, err.message);
  }
};

export const deleteFoodService = async (req, res) => {
  try {
    await res.foodService.remove();
    res.json({ message: 'Food service deleted' });
  } catch (err) {
    sendServerError(res, err.message);
  }
};
