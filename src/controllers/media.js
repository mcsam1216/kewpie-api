import fs from 'fs';
import Media, { referenceTypes, mediaTypes } from '../model/media';
import { saveFile } from '../config/file-upload';
import config from '../config';
import { sendBadRequest } from '../helpers/response';

const productUpload = `${config.file.upload}/products/`;

export const checkProductImages = async (req, res, next) => {
  try {
    const { files: { productImages } } = req || undefined;
  
    if (!productImages) {
      throw new Error('No product\'s image(s) found');
    } else {
      req.productImages = productImages;
    }
  } catch (err) {
    throw new Error(err.myessage);
  }
  
  next();
};

export const removeProductMedia = async (req, res, next) => {
  try {
    const { product: { _id }, productImages } = res;
  
    for (const image of productImages) {
      fs.unlinkSync(`${productUpload}${image.mediaPath}`);
    }
    
    await Media.deleteMany({ referenceID: _id });
  } catch (err) {
    return sendBadRequest(res, err.message);
  }
  next();
};

export const createNewProductMedia = async (productID, productImages) => {
  try {
    const saveSingleFile = async (image) => {
      const imageName = `${Date.now()}${image.name}`;
      await saveFile(image, `${productUpload}${imageName}`);
  
      const media = new Media({
        referenceID   : productID,
        referenceType : referenceTypes.PRODUCT,
        mediaType     : mediaTypes.IMAGE,
        mediaPath     : imageName,
      });
  
      await media.save();
    };

    if (Array.isArray(productImages)) {
      for (const image of productImages) {
        await saveSingleFile(image);
      }
    } else {
      await saveSingleFile(productImages);
    }
  } catch (err) {
    throw new Error(err.message);
  }
};

export const getProductImages = async (referenceID) => {
  try {
    const media = await Media.find({
      referenceID,
      referenceType : referenceTypes.PRODUCT,
      mediaType     : mediaTypes.IMAGE,
    });

    return media;
  } catch (err) {
    throw new Error(err.message);
  }
};