import FoodServiceCategory from '../model/product-category';
import { sendBadRequest, sendServerError } from '../helpers/response';

export const getFoodServiceCategories = async (req, res) => {
  try {
    const foodServiceCategory = await FoodServiceCategory.find();
    res.json(foodServiceCategory);
  } catch (err) {
    sendServerError(res, err.message);
  }
};

export const postFoodServiceCategory = async (req, res) => {
  const { body } = req;
  const foodServiceCategory = new FoodServiceCategory(body);

  try {
    const newCategoryProduct = await foodServiceCategory.save();
    res.status(201).json(newCategoryProduct);
  } catch (err) {
    sendBadRequest(res, err.message);
  }
};

export const updateFoodServiceCategory = async (req, res) => {
  for (const prop in req.body) {
    res.foodServiceCategory[prop] = req.body[prop];
  }

  try {
    const updatedFoodServiceCategory = await res.foodServiceCategory.save();
    res.json(updatedFoodServiceCategory);
  } catch (err) {
    sendBadRequest(res, err.message);
  }
};

export const deleteFoodServiceCategory = async (req, res) => {
  try {
    await res.foodServiceCategory.remove();
    res.json({ message: 'Food service\'s category deleted' });
  } catch (err) {
    sendServerError(res, err.message);
  }
};

export const getFoodServiceCategory = async (req, res, next) => {
  let foodServiceCategory;
  try {
    foodServiceCategory = await FoodServiceCategory.findById(req.params.id);
    if (foodServiceCategory === null) {
      return res
        .status(404)
        .json({ message: 'No Food Service\'s Category found.' });
    }
  } catch (err) {
    return sendServerError(res, err.message);
  }

  res.foodServiceCategory = foodServiceCategory;
  next();
};
